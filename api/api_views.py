from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework import status
from home.models import UserTool, Tool
from .serializers import UserToolGetSerializer, UserToolPostSerializer, ToolSerializer


class UserToolModelViewSet(ModelViewSet):
    """
    Fetches a user's tools, posts new tools
    """
    queryset = UserTool.objects.all()
    serializer_class = UserToolGetSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return UserTool.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return UserToolPostSerializer
        return UserToolGetSerializer


class ToolUsageUpdateView(APIView):
    """
    Updates a tool's usage
    """
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication]

    def get_tool(self, user, pk):
        try:
            return UserTool.objects.get(user=user, id=pk)
        except UserTool.DoesNotExist:
            return False

    def post(self, request):
        tool = self.get_tool(request.user, request.data.get('pk'))
        if not tool:
            return Response("That tool does not exist!", status=status.HTTP_400_BAD_REQUEST)
        tool.uses += 1 if request.data.get('action') == 'incr' else -1
        if tool.uses < 0:
            tool.uses = 0
        tool.save()
        if tool.uses >= tool.tool.durability:
            tool.delete()
        return Response("Updated", status=status.HTTP_202_ACCEPTED)


class GetUpgradeChoices(APIView):
    """
    Gets appropriate options for upgrading a tool
    """
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication]

    @staticmethod
    def get_options(category, current):
        return Tool.objects.filter(category=category).exclude(id=current)

    def post(self, request):
        options = self.get_options(request.data.get('category'), request.data.get('current'))
        serializer = ToolSerializer(options, many=True)
        return Response(serializer.data)
