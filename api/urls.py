from django.urls import path
from rest_framework.routers import DefaultRouter
from .api_views import UserToolModelViewSet, ToolUsageUpdateView, GetUpgradeChoices

router = DefaultRouter()
router.register('tools', UserToolModelViewSet)

urlpatterns = [
    path('usage/', ToolUsageUpdateView.as_view(), name='tool-usage'),
    path('choices/', GetUpgradeChoices.as_view(), name='upgrade-choices')
] + router.urls
