from rest_framework import serializers
from home.models import UserTool, Tool


class UserToolGetSerializer(serializers.ModelSerializer):
    display_name = serializers.CharField(source='tool.get_name_display')

    class Meta:
        model = UserTool
        fields = '__all__'
        depth = 1


class UserToolPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTool
        fields = '__all__'


class ToolSerializer(serializers.ModelSerializer):
    display_name = serializers.CharField(source='get_name_display')

    class Meta:
        model = Tool
        fields = '__all__'
