from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .models import Tool


class HomeView(TemplateView):
    template_name = 'home/home_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        choices = []
        for category in Tool._meta.get_field('category').choices:
            choices.append({'header': category[0]})
            for tool in Tool.objects.filter(category=category[0]):
                choices.append({'text': str(tool), 'value': tool.id})
        context['tool_choices'] = choices
        return context


def signup(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})
