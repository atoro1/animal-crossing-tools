from django.db import models
from django.contrib.auth.models import User


TOOL_NAME_CHOICES = [
    ('Axe', (
        ('flimsy_axe', 'Flimsy Axe'),
        ('stone_axe', 'Stone Axe'),
        ('normal_axe', 'Axe'),
        ('golden_axe', 'Golden Axe')
    )
     ),
    ('Fishing Rod', (
        ('flimsy_fishing_rod', 'Flimsy Fishing Rod'),
        ('normal_fishing_rod', 'Fishing Rod'),
        ('colorful_fishing_rod', 'Colorful Fishing Rod'),
        ('fish_fishing_rod', 'Fish Fishing Rod'),
        ('outdoorsy_fishing_rod', 'Outdoorsy Fishing Rod'),
        ('golden_fishing_rod', 'Golden Fishing Rod')
    )
     ),
    ('Net', (
        ('flimsy_net', 'Flimsy Net'),
        ('normal_net', 'Net'),
        ('colorful_net', 'Colorful Net'),
        ('outdoorsy_net', 'Outdoorsy Net'),
        ('star_net', 'Star Net'),
        ('golden_net', 'Golden Net')
    )
     ),
    ('Shovel', (
        ('flimsy_shovel', 'Flimsy Shovel'),
        ('normal_shovel', 'Shovel'),
        ('colorful_shovel', 'Colorful Shovel'),
        ('outdoorsy_shovel', 'Outdoorsy Shovel'),
        ('printed_design_shovel', 'Printed-Design Shovel'),
        ('golden_shovel', 'Golden Shovel')
    )
     ),
    ('Slingshot', (
        ('normal_slingshot', 'Slingshot'),
        ('colorful_slingshot', 'Colorful Slingshot'),
        ('outdoorsy_slingshot', 'Outdoorsy Slingshot'),
        ('golden_slingshot', 'Golden Slingshot')
    )
     ),
    ('Watering Can', (
        ('flimsy_watering_can', 'Flimsy Watering Can'),
        ('normal_watering_can', 'Watering Can'),
        ('colorful_watering_can', 'Colorful Watering Can'),
        ('elephant_watering_can', 'Elephant Watering Can'),
        ('outdoorsy_watering_can', 'Outdoorsy Watering Can'),
        ('golden_watering_can', 'Golden Watering Can')
    )
     )
]


class Tool(models.Model):
    class ToolCategory(models.TextChoices):
        AXE = 'Axe'
        FISHING_ROD = 'Fishing Rod'
        NET = 'Net'
        SHOVEL = 'Shovel'
        SLINGSHOT = 'Slingshot'
        WATERING_CAN = 'Watering Can'

    name = models.CharField(max_length=50, choices=TOOL_NAME_CHOICES)
    category = models.CharField(max_length=20, choices=ToolCategory.choices)
    durability = models.IntegerField()
    image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.get_name_display()


class UserTool(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)
    uses = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.user.username}'s {self.tool}"
