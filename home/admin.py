from django.contrib import admin
from .models import Tool, UserTool


@admin.register(Tool)
class ToolAdmin(admin.ModelAdmin):
    list_display = ('name', 'durability')
    list_editable = ('durability',)


@admin.register(UserTool)
class UserToolAdmin(admin.ModelAdmin):
    list_display = ('user', 'tool', 'uses')
