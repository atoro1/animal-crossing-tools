from django.core.management.base import BaseCommand
from home.models import Tool

durability_dict = {
    # Axes
    'flimsy_axe': 40,
    'stone_axe': 100,
    'normal_axe': 100,
    'golden_axe': 200,

    # Fishing rods
    'flimsy_fishing_rod': 10,
    'normal_fishing_rod': 30,
    'colorful_fishing_rod': 30,
    'fish_fishing_rod': 30,
    'outdoorsy_fishing_rod': 30,
    'golden_fishing_rod': 90,

    # Nets
    'flimsy_net': 10,
    'normal_net': 30,
    'colorful_net': 30,
    'outdoorsy_net': 30,
    'star_net': 30,
    'golden_net': 90,

    # Shovels
    'flimsy_shovel': 40,
    'normal_shovel': 100,
    'colorful_shovel': 100,
    'outdoorsy_shovel': 100,
    'printed_design_shovel': 100,
    'golden_shovel': 200,

    # Slingshots
    'normal_slingshot': 20,
    'colorful_slingshot': 20,
    'outdoorsy_slingshot': 20,
    'golden_slingshot': 60,

    # Watering cans
    'flimsy_watering_can': 20,
    'normal_watering_can': 60,
    'colorful_watering_can': 60,
    'elephant_watering_can': 60,
    'outdoorsy_watering_can': 60,
    'golden_watering_can': 60
}


class Command(BaseCommand):
    help = 'Creates all tool combinations for easier setup'

    def handle(self, *args, **options):
        options = Tool._meta.get_field('name').choices
        for category, option_list in options:
            for value, name in option_list:
                Tool.objects.create(name=value, category=category, durability=durability_dict[value])
